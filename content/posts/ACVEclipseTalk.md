---
title: "ACVEclipseTalk"
date: 2024-04-03T17:35:50-04:00
draft: false
---

Since my Rochester townhouse is right in the path of totality for the
April 8, 2024 total solar eclipse I (foolishly?) volunteered to give a
talk on the eclipse for my HOA.

This is where I'm stashing [the powerpoint file](https://docs.google.com/presentation/d/1-oPjxXQDzsksxwHraEWsGxxkieK3n3VY/edit?usp=sharing&ouid=115754775200839587802&rtpof=true&sd=true) from that talk.  Warning! 1.47GB File!

Here is a slightly older, lower resolution version of [that same presentation.](https://docs.google.com/presentation/d/12XPyFl2o1cV_tKDeTQY5tqR_AW0lMxjB/edit?usp=sharing&ouid=115754775200839587802&rtpof=true&sd=true) This file is 'only' 555MB.

Also for your convenience, here are the links from the final slide of the presentation. (Many of these are updated live, so you will see newer info than from what was in the talk.)

1. The astro 101 video from Jason Kendall: https://www.youtube.com/watch?v=pcqhHZ5VYn8

1. The PBS presentation (for both what to look for and the pinhole camera construction): https://www.youtube.com/watch?v=791qJZivHpk

1. The Great American Eclipse; the website itself: https://www.greatamericaneclipse.com/ and the flyover video: https://www.youtube.com/watch?v=sOpYoO_SK7o

1. The astronomical cloud forecast from the Canadian Meteorological Centre. The website itself: https://weather.gc.ca/astro/
 and the forecast cloud videos: https://weather.gc.ca/astro/clds_vis_e.html

1. The ECMWF ('European') ensemble model cloud forecast for Rochester: https://www.yr.no/en/details/table/2-5134086/United%20States/New%20York/Monroe/Rochester

1. The Clear Dark Skies website;  the eclipse map https://www.cleardarksky.com/ec/2024-04-08_eclipse_map.html and the page for the RIT observatory https://www.cleardarksky.com/c/RchstrHYkey.html

1. The Rochester Museum and Science Center's eclipse website: https://rochestereclipse2024.org/ -- a good place to get reliable eclipse glasses locally if you haven't already secured yours.

1. Somebody (Marianne?) asked about where to possibly see the comet (12P Pons-Brooks). It's actually quite faint (magnitude 4 or so, unless it happens to have another gas outburst). But [here is a chart](https://www.heavens-above.com/skychart.ashx?size=800&FOV=60&innerFOV=10&MaxMag=5&RA=2.70346078287655&DEC=18.180121466116&mjd=60408.9036921296&cn=1&cl=1&cul=en). The comet is denoted on the chart with a little fantail. Jupiter should be very evident so look nearby. Honestly, the eclipse itself might be more of an attraction, but it might be worth a few seconds scan looking for the comet.

1. Somebody else (I forget who) asked about online places to see the eclipse. This Sky and Telescope [page](https://skyandtelescope.org/astronomy-news/where-to-watch-the-total-solar-eclipse-online/) lists several options.

1. Last but not least, here is the grande dame of amateur astronomy, Sky and Telescope magazine's [webpage on the eclipse](https://skyandtelescope.org/total-solar-eclipse-2024/).

Good Luck and Clear Skies!


