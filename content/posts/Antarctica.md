---
title: "Antarctica"
date: 2025-02-27T13:52:51+08:00
draft: false
---

![Me at Damoy Point, Antarctica!](/FrankSite/Antarctica/MeAtDamoyPoint.jpg
"Me, front and center. The red hut is a former British Antarctic
Survey installation at Damoy Point. Photo credit: Pancho Gomez.")  
(Most of the more
gorgeous photos in this post are from the expedition's photographer,
Pancho Gomez. If a mouseover of the photo doesn't credit him,
photographic credit is either due to myself, my sister Susy, or my
roommate Steve Doucette.)

So, about 18 months ago my sister Susy asked me if I wanted to join
her and a bunch of (mostly) her dive buddies on a trip to Antarctica.

For me, the opportunity to bag my sixth (South America) and seventh
continent (Antarctica) was irresistible. It took me milliseconds to
make that decision for myself. However, when I asked Pam if she wanted
to join us. The answer was a very definite _nope_. I continued to ask
her to join many times in the intervening months, but her answer was
always something along the lines of "I'm too old". Whatever. (There
was an 82 year old along on our trip. Yes, she struggled a bit, but
she managed.) A Qantas agent on my way back had the best line about
that: "Silly Girl!" 😀

Life threw us many curveballs in the intervening time -- including a
serious cancer scare for Pam. (As far as we are aware at the time I am
writing this, she is now cancer-free!)

But the time for my departure arrived. We had just arrived at our
house in Busselton, W.A. -- after more than 48 hours in transit from
the States -- a few days prior to me needing to get to Punta Arenas,
Chile which was the departure point for my fly/cruise expedition. So,
after packing, weighing, and re-packing all of my gear multiple times,
I drove back up to Perth with the rental car from the previous week --
leaving Pam with our car at our house.

- Busselton to Perth: 2.5 hours by road.

- Perth to Sydney: 5 hours on Qantas.

- Sydney to Santiago: 15-16 hours on LATAM. (My first experience with that airline, and I can highly recommend them!)

- Santiago to Punta Arenas: 3.5 hours on LATAM. (Chile is *big*.)

To say that I was seriously jet-lagged would be an understatement.

Before you ask, I only have a tiny Spanish vocabulary. I made do after
having downloaded the Spanish language pack for Apple Translate on my
iPhone. It worked well enough for when I needed to communicate some
mildly complicated concepts, but I mostly got by with saying "gracias"
and smiling a lot. All of the important transactions -- like checking
in to hotels or dealing with taxi/Uber drivers I mostly lucked out
with people who spoke enough English to accomplish the task.

![Susy Horowitz and Steve Doucette](/FrankSite/Antarctica/SteveAndSusy.JPG
 "Susy and Steve.")

Once I met up with the group, I was in for a big surprise. Susy's
diving buddy Steve Doucette was my roomie in the shared cabin. As it
turned out, Steve was in
[Clem Nelson](https://epss.ucla.edu/news/clem-nelson-peak/)'s geo 1
class at UCLA about a year or so after I took it in the mid
'70s. Steve knew instantly that I was talking about Clem when I said
that he was probably the (second?) best _teacher_ I had ever
encountered. A perfect choice in roomies, and we got on like a house
on fire. In tribute to Clem, on our last day aboard I re-told some of
his campfire jokes. No, they are not suitable for polite company
anymore -- nor were they particularly suitable back then. But "Holy
Mackerel" are they still funny even after all this time. (Clem passed
away about 20± years back.)

![BAE 146-200](/FrankSite/Antarctica/FlyingPenguin.jpg "The penguin
 that flies. Photo credit: Pancho Gomez.")


Flying to Antarctica from Punta Arenas is 100% controlled by the
weather at the (Chilean) Frei airbase on King George Island. The DAP
(Antarctic Airlines) plane was some specialized 4-engined jet (BAE
146-200?) capable of taking off from very short airstrips -- like the
unpaved runway at Frei. We were on standby at the hotel in Punta
Arenas from about midnight on the day we were supposed to fly until
about 3 p.m. when we actually took off. We needed to be ready with 2
hours notice.

![Zodiac Landing, Port Lockroy, Antarctica!](/FrankSite/Antarctica/ZodiacLandingPortLockroy.jpg
 "The hut is Port Lockroy, the northernmost post office in
 Antarctica. Photo credit: Pancho Gomez.")

All transport between any shore destination -- including Frei airbase
-- and our ship (Ocean Nova) was by Zodiac. As a sailor, what worried
me most was the 0-3 °C water splashing and soaking everything
once aboard a zodiac, and that is what I mostly geared up for. (Thanks
to the great deities of mariners, there's a West Marine in Rochester,
NY! I got a decent pair of waterproof winter ships gloves, and they
were perfect.) Other than that, the temperatures for our excursions
were mostly just around freezing -- actually warmer than upstate NY in
winter!

![Ocean Nova](/FrankSite/Antarctica/OceanNova.jpg "The M/V Ocean
 Nova. Photo credit: Pancho Gomez.")

The M/V Ocean Nova; she's a grand old gal, originally built in 1991 to
be a ferry in Greenland waters, but re-fit to be a cold-water cruise
ship in about 2000 at the same Gdańsk shipyard made famous by
Lech Wałęsa and Solidarność. She holds
about 65-75 passengers, plus about half of that again as ship and
"hotel" crew, and about another 15-ish of the expedition staff from
the [Antarctica 21](https://www.antarctica21.com/) outfit that ran the
trip -- another highly recommended group!

My life aboard was centered around the viewing lounge and deck just
above the ships bow. If you see glass in any of my photos, there's a
good chance it was taken from that lounge. Dining was in a separate
room. (I thought that the food aboard was great!)

We only had 5 nights aboard the ship. Time passed very fast! We had
about two shore excursions per day, but once again Mother Nature
controlled everything about the locations. Our expedition leader,
David Berg -- from Sweden -- has at least a decade (probably much
more) with those conditions. Hour-by-hour, he was deciding where we
could go ashore based on wind and snow forecasts. You do not want to
take a bunch of green tourists (like us) ashore in zodiacs with 25 kt
winds and whitecaps. Certainly not in water that cold.

![Itinerary/Route](/FrankSite/Antarctica/AntarcticaItinerary.jpg
 "Locations for the cruise, with schematic route overlain.")

The image above shows a schematic map of our voyage. I've overlaid our
route on the base map provided by Antarctica 21.

![Course plot slide](/FrankSite/Antarctica/CoursePlotSlide.jpeg "A
 better map, but a poorer image.")

Here's the more accurate chart, but poorly photographed by yours
truly.

For a slide show of the voyage photographed by people with *far* more
talent than me [click here](https://drive.google.com/file/d/1jkSwdtvtdwbRl2PCxEN9Xa9Kk7Zw-kFY/view?usp=sharing). Most
of the credit for those photos goes to Pancho Gomez of the expedition
staff -- who also put together the slide show.

![Gentoo on Antarctic Mainland](/FrankSite/Antarctica/GentooOnAntarctica.JPG
 "Sitting by the dock of the bay...")

![Gentoo!](/FrankSite/Antarctica/Gentoo.JPG "More Gentoos!")

![Gentoo Chicks!](/FrankSite/Antarctica/GentooChicks.JPG "Gentoo Chicks! Just about finished with their moult.")

![Chinstrap](/FrankSite/Antarctica/Chinstrap.jpg "Chinstrap penguin. Photo credit: Pancho Gomez.")

We saw LOTS of penguins. Wendy Hare is a Kiwi expedition
staff/naturalist aboard who told us how to identify the species we
were likely to encounter. I personally mostly saw Gentoo penguins
(with a red stripe on their beaks) and a few chinstraps. The one shore
excursion I stayed aboard for (editing a video for my TCF friends) was
to a colony of chinstraps.

![Gentoos Swimming](/FrankSite/Antarctica/GentoosSwimming.jpg "Gentoos
 Swimming. Photo credit: Pancho Gomez.")

Not only did we see penguins onshore, but also rafts and rafts of them
"dolphining" in the water. Those birds are FAST swimmers.

![(Humpback?) Whale Belly](/FrankSite/Antarctica/WhaleBelly.jpg
 "Humpback (maybe) whale belly. Photo credit: Pancho Gomez.")

There were lots of humpback whales around. I lost count of how many I
saw! I also saw a few fin whales, and minke. Plus, there was a pod of
about 15-ish Orca
([with my amateurish video clips here](https://drive.google.com/file/d/1Wbi9q3i4TEGfufrXlYrusxeHcGH08U_m/view?usp=sharing))
that appeared one afternoon. (There are much better images/videos of
the Orca in the slideshow I pointed to above.) The expedition staff
said that these were the first Orca they had seen this season -- which
is coming to a close in a few weeks.

There were birds (other than penguins) galore: albatross, seagulls,
skua, various types of terns. It's a birdwatcher's paradise!

![Bart Simpson, trapped in a iceberg!](/FrankSite/Antarctica/BartIcebergCropped.jpeg "Poor Bart is trapped. Watch it, dude!")

We saw many, many icebergs -- including one that I thought looked like
Bart Simpson.

![Rotten Ice at Glacier Front!](/FrankSite/Antarctica/GlacialCalvingFront.JPG "Rotten Ice!")

We saw (and heard) many glacial calving events. The above photo is of
the rotten ice at the front of a glacier.

![Zodiac near Glacier Front!](/FrankSite/Antarctica/ZodiacNearIceFront.JPG "Zodiacs Beware!")

Our zodiac drivers were quite wary of being trapped in floating ice
too close to the fronts of the glaciers in case we had to outrun a
calving wave.  Unexpectedly for me, we also saw floating dirty ice --
where rocks and other sediments from moraines were kept afloat by the
ice density.

![Seal tired of U-shaped Valley lecture!](/FrankSite/Antarctica/SealTiredOfUShapedValleyLecture.JPG "BORING!")

I gave the "U-shaped vs. V-shaped valley" lecture multiple times! This seal
was tired of the lecture. "What do **YOU** know about ice?"

In addition to Steve, there were several other earth scientists (5 or
so, depending on who you want to tar with that brush) just in Susy's
diving buddy group. I enjoyed being able to talk shop with like minded
folks! There was also an ESA applied physicist who works on satellite
instruments -- and her bright spark kids.

Finally, for my [TCF](https://compassionatefriends.org) friends, I
happened to meet a Mexican family onboard who lost their son/brother
at age 28 five years ago. I felt an instant kinship and bond with
them. For some of my TCF tribe back home, I also did an impromptu
"penguin waddle to remember" where I read the names of some 140
children who had died. (For those of my friends fortunate enough to
not know about this stuff, surviving family members take great comfort
in hearing somebody say the names of their loved ones.) If you are a
TCF person who wants to see the video, contact me, and I'll send you a
link.

It's now been eleven days since I returned home, and I'm tired of
slowly adding to and polishing this post. So, please enjoy a dump of
many of my other half-decent photos. They are in rough chronological
order. N.B. There are an unrepresentative number from Port Lockroy
below, mostly because I happened to be shutter-happy that day. Also,
geophysicist me likes the IGY/ionospheric connection. Captions are
fubar at the moment so mouse-over for captions. 



![Andean Volcano](/FrankSite/Antarctica/GorgeousAndeanVolcanoWithTruncatedTop.jpeg "A gorgeous volcano in the Andes, about 30-40 minutes airplane ride south of Santiago on the way to Punta Arenas. The only non-Antarctic photo in this post.")
![Poster explaining the geology of the Bransfield Strait](/FrankSite/Antarctica/BransfieldStraitGeology.JPG "Poster onboard the ship. It's a spreading center!")
![Active Argentine base at Cierva Cove](/FrankSite/Antarctica/ArgentineStationCiervaCove.JPG "An active Argentine base at Cierva Cove. The zodiac driver said sometimes they could smell the BBQs from the base.")
![Leopard Seal! Cierva Cove](/FrankSite/Antarctica/LeopardSeal.JPG "My first leopard seal. Cierva Cove.")
![Steve Doucette](/FrankSite/Antarctica/SteveDoucette.JPG "My roomie Steve Doucette. He just happened to be ex-UCLA geo dept by a pure coincidence!")
![Gentoo Penguin colony with mountain background ](/FrankSite/Antarctica/PenguinColonyWithMtnBackground.JPG "A gentoo penguin colony, with a gorgeous mountain for background!")
![Same mountain, with U shaped valley](/FrankSite/Antarctica/GlacierUShapedValley.JPG "More of the same mountain, with a U-shaped glacial valley.")
![Gentoo penguin chicks moulting under hut at Damoy point](/FrankSite/Antarctica/PenguinChicksUnderDamoyHut.jpeg "Cute -- moulting -- gentoo penguin chicks under the hut at Damoy point.")
![Seal on Iceberg](/FrankSite/Antarctica/SealOnIceberg.JPG "Just sealing around.")
![Same seal: you again?](/FrankSite/Antarctica/SealOnIceberg1.JPG "You again?")
![Same seal yawning](/FrankSite/Antarctica/SealBeautyRest.JPG "Please be gone! I'm tired of you!")
![A proto terminal morraine](/FrankSite/Antarctica/ProtoTerminalMorraine.JPG "What a terminal morraine looks like before it is dumped.")
![Port Lockroy plaque](/FrankSite/Antarctica/PortLockroyBaseAPlaque.JPG "A plaque at Port Lockroy. Formerly a major station for the British Antarctic Survey. Now a museum, and 'the northernmost post office on Antarctica'.")
![Meteorological instruments, Port Lockroy](/FrankSite/Antarctica/PortLockroyMetInstruments.jpeg "Old meteorological(?) instruments at Port Lockroy.")
![A portrait of Prince Philip who visited Port Lockroy in 1957](/FrankSite/Antarctica/PrincePhilip1957.jpeg "Prince Philip visited Port Lockroy the year I was born -- which was also the IGY.")
![HRH Queen Elizabeth II is still celebrated at Port Lockroy](/FrankSite/Antarctica/PortLockroyQEIIjpeg.jpeg "I was chuffed to see that Queen Elizabeth was still celebrated at Port Lockroy.")
![Port Lockroy living quarters hut](/FrankSite/Antarctica/PortLockroy.jpeg "The current living quarters at Port Lockroy. No water supply other than what's brought in by passing tourist ships. No showers. Penguin colony in the foreground. ")
![Port Lockroy radio room](/FrankSite/Antarctica/PortLockroyRadioRoom.jpeg "Radio Room at Port Lockroy.")
![Port Lockroy barometer](/FrankSite/Antarctica/PortLockroyBarometer.jpeg "A barometer at Port Lockroy. Reminds me of the barometer in the chem lab building at UCLA.")
![](/FrankSite/Antarctica/IonosphericWhistlersPortLockroy.jpeg "Early equipment for recording ionosphereic whistlers at Port Lockroy.")
![A blurb about "The Beastie" ionospheric sounder at Port Lockroy](/FrankSite/Antarctica/PortLockroyTheBeastieBlurb.jpeg "A blurb to explain 'The Beastie' at Port Lockroy.")
![The Beastie itself; Port Lockroy](/FrankSite/Antarctica/PortLockroyIonosphericTheBeastie.jpeg "The Beastie itself; an ionospheric sounder at Port Lockroy.")
![The ionospheric room; Port Lockroy](/FrankSite/Antarctica/PortLockroyIonosphericRoom.jpeg "The ionospheric room at Port Lockroy.")
![The Bark Europa, in the bay behind Port Lockroy](/FrankSite/Antarctica/BarkEuropa.jpeg "The Bark Europa in the bay behind Port Lockroy.")
![Expedition photographer Pancho Gomez in short sleeves: summer in Antarctica](/FrankSite/Antarctica/SummerInAntarcticaPanchoShortSleeves.jpeg "Summer in Antarctica. Note the short sleeves on photographer Pancho Gomez!")
![Antarctic sunset](/FrankSite/Antarctica/AntarcticSunset.jpeg "An Antarctic sunset from the Ocean Nova.")
![Sea stacks at Deception Island](/FrankSite/Antarctica/SeaStacksDeceptionIsland.jpeg "Sea stacks at Deception Island!")
![Departure sunrise; King George Island](/FrankSite/Antarctica/DepartureSunriseKingGeorgeIsland.jpeg "Sunrise at King George Island while we were waiting for our departure plane.")
