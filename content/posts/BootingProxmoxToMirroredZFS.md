---
title: "Booting Proxmox To Mirrored ZFS with Dissimilar Drive Sizes"
date: 2022-03-13T22:09:58-04:00
draft: false
---

I've been using [ZFS on Linux](https://zfsonlinux.org/) for years now, because it is a *great* filesystem. But I've always found one thing to be in the "too hard" basket. (A *great* Aussie concept!) Booting into a "/" (root) which is mounted as a ZFS filesystem has always been for me the proverbial maze of twisty passages, all alike. Ubuntu seems to have solved that root-on-zfs problem, and I actually have a machine booting Ubuntu into mirrored root. 

But, I wanted a new machine I am commissioning to run [Proxmox](https://www.proxmox.com/en/) -- a Debian based hypervisor that's kinda like a "poor geeks VMWare Esxi". The stock Proxmox installer actually has an option to do a mirrored root. 

But I had a twist. (Of course, or I wouldn't need to write this post.) I was trying to build a ZFS mirror out of one whole disk device, and one larger disk with a partition created to match the exact size of the smaller whole disk. The Proxmox mirrored installer kept trying to use both whole disks, and balked at my trying to use a partition smaller than the whole disk. In Proxmox's defense, that usage is not considered best practice for ZFS, so I am not actually too terribly annoyed at Proxmox. (Really! ;-) )

After scratching my head for a while -- and trying several other contortions to work around the issue-- a quick [DDG](https://duckduckgo.com) search led me to remember that [ZFS/zpool has an "attach" command](https://openzfs.github.io/openzfs-docs/man/8/zpool-attach.8.html). That incantation allows you to *create* a mirror out of a singleton device.

Soooo, problem solved. I installed Proxmox root-on-zfs onto the smaller disk, built the matching partition size on the larger disk, used the zpool attach command to mirror the smaller disk and the partition, *et voilà*! The resulting duct-taped (**DUCT TAPE!!!**) hack boots just fine, and I'm a happy camper. 