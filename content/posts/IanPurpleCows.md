---
title: "Ian and Purple Cows"
date: 2022-05-14T12:06:02-04:00
draft: false
---

Pam asked me to find some old photos this morning of a house in
Pennsylvania that one of her ancestors built back in the middle ages
(or something). While trawling through my archives, I found this
photo from nearby PA:

![Purple Cow Ice Cream!](/FrankSite/IanPurpleCows/ThePurpleCow.jpg "Purple Cows, Purple Cowwwws...")

Therein lies a tale.

When our late son Ian was a wee little tyke in Oz in the early to mid
'90s, we used to take longish road trips. Sitting in the back seat for
such long times is boring for kids, as I'm sure everybody remembers
well. I used to try to liven things up a little everytime we passed a
herd (murder? pride?)  of cows by telling Ian to be on the lookout for
"Purple Cows".

Every so often I'd say "Look! There's a purple cow!!"  but of
course he never saw one. I then teased him that purple cows liked to
hide behind trees.

Occasionally, I'd break into a deliberately off-key chorus -- to the
tune of Prince's Purple Rain:

*Purple cows, purrrrple cowwwwwsss. La de dah de dah de dah.*

That would often cause Ian to break out in the giggles, and my mission
as "baby entertainer" was accomplished for maybe the next half hour or
so of driving.

Of course, neither one of us ever saw a purple cow.

That is, never until the day in 2013 when Pam and I were driving
around rural Pennsylvania looking for her ancestor's house. Amazingly,
what do I spy?  A roadside ice-cream stand named "The Purple Cow".

You all know what I had to do...

I miss my boy.
