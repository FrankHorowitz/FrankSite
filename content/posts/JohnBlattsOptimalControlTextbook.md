---
title: "John Blatt's 1981 Textbook on Optimal Control"
date: 2022-02-27T16:38:39-05:00
draft: false
---
# Putting John Blatt's 1981 text back online

This post serves as the restoration of a site I kept online in various places from 2005 until about 2015 or so. To the best of my knowledge, this copy might be the only legal way to read that textbook -- unless you happen to have a paper copy on your bookshelf.

I'm reproducing the relevant text and links below, in the hope that the textbook will be of use to anybody who finds this.

Prof. John Blatt was, by all accounts "quite a character". I never met him, but [this obituary](https://web.maths.unsw.edu.au/~jim/blatt.html) was written by someone who was in the same maths dept. My late colleague, Peter Hornby, had the good fortune to be in Prof. Blatt's optimal control class as a Ph.D. student at UNSW in the '80s, and had a copy of his book in paper form on his bookshelf. That is the source of the scanned document below.

N.B. Peter Hornby passed away in 2009. The permission to publish the copy-written text -- conferred on us by Prof. Blatt's widow as documented below -- hence has devolved solely to me as the survivor. I am reproducing the work under the auspices of that permission, and make **_no claim_** to copyright over the text. I am merely supporting its online distribution.

In our work Peter and I used Blatt's suggested Hamiltonian based algorithm fairly extensively to build interesting non-traditional optimal control problems -- e.g. to design renewable energy systems. A student and I have even had a try at using the approach to design geothermal energy systems using money as an objective function. I think, for a topic that was *hot* in the decade in which I was born (1950s) there's a fair amount of weird-and-wonderful applications mileage left in this topic, if you think about "optimal control" abstractly enough.

Please be aware: Peter Hornby and (now) I alone have documented permission to distribute the text for free. If you wish to have a friend or colleague read the text, *great!* Please just honor the spirit of Ruth Blatt's generosity and point them towards this website to download a copy instead of giving them a copy yourself.

--- Frank Horowitz 27 Feb. 2022


# Original Website Text

We have received formal permission to reproduce (on a not-for-profit basis) the text "Lecture Notes Containing an Elementary Introduction to Optimal Control" from Prof. John M. Blatt's widow, Mrs. Ruth Blatt, copyright holder of the work. 

A PDF of the scanned text is [available](/FrankSite/BlattText/BlattAllScanned.pdf), and the written correspondence supporting our permission to reproduce the work is also attached to this page. 

| Letter | Description|
|----|-----|
|[David Blatt Letter](/FrankSite/BlattText/DavidBlattLetter.jpg)|Inital letter to David W. E. Blatt, chasing down copyright owner.|
|[Ruth Blatt Letter](/FrankSite/BlattText/RuthBlattLetter.jpg)|Correspondence with Copyright holder Ruth Blatt requesting permission to reproduce.|
|[Ruth Blatt Reply](/FrankSite/BlattText/RuthBlattReply.jpg)|Ruth Blatt's reply authorizing us to reproduce the work on a not-for-profit basis.|

--- Frank Horowitz 17 May 2005

